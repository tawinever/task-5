import java.util.HashSet;
import java.util.Set;

public class PizzaPack {
    static int MAX_CAPACITY = 7;
    private String name;
    private PizzaType pizzaType;
    private Set<Ingredient> ingredients;
    private int quantity;
    private boolean isNameCustom = false;

    PizzaPack(String name, PizzaType pizzaType, int quantity) {
        if (name != null && name.length() >= 4 && name.length() <= 20) {
            this.name = name;
            this.isNameCustom = true;
        } else {
            this.name = "waitingUpdate";
        }
        this.pizzaType = pizzaType;
        setQuantity(quantity);
        this.ingredients = new HashSet<>();
    }

    public void setQuantity(int quantity) {
        if (quantity > 10) {
            System.out.println("Maximum pizza quantity is 10");
        }
        this.quantity = Math.min(quantity, 10);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String alternativeName) {
        if (!isNameCustom) {
            this.name = alternativeName;
        }
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void addIngredient(Ingredient ingredient) {
        if (ingredients.contains(ingredient)) {
            System.out.println("Check order again");
            return;
        }

        if (ingredients.size() == MAX_CAPACITY) {
            System.out.println(String.format("Pizza is already full. There are %d ingredients out of %d",
                    MAX_CAPACITY, MAX_CAPACITY));
            return;
        }

        ingredients.add(ingredient);
        System.out.println(String.format("Added %s to %s", ingredient.toString(), this.name));
    }

    public Set<Ingredient> getIngredients() {
        return this.ingredients;
    }

    public PizzaType getPizzaType() {
        return this.pizzaType;
    }

    public double getSum() {
        double sum = 0;
        for (Ingredient ingredient : Ingredient.values()) {
            if (this.ingredients.contains(ingredient)) {
                sum += Price.getIngredientPrice(ingredient);
            }
        }
        sum += Price.getBasePrice(this.getPizzaType());
        return sum;
    }
}
