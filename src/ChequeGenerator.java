public class ChequeGenerator {
    private LineGenerator lineGenerator;

    ChequeGenerator() {
        this.lineGenerator = new LineGenerator();
    }

    private String drawTitle(Order order) {
        return  String.format("Заказ: %d", order.getOrderId()) + "\n" +
                String.format("Клиент: %d", order.getClientId()) + "\n";
    }

    private String drawPizzaPack(PizzaPack pizzaPack) {
        StringBuilder res = new StringBuilder();
        res.append(String.format("Название: %s", pizzaPack.getName())).append("\n");
        res.append(this.lineGenerator.divider());

        res.append(this.lineGenerator.floatToEnd(
                String.format("Pizza Base (%s)", pizzaPack.getPizzaType().toString()),
                String.format("%.2f €", Price.getBasePrice(pizzaPack.getPizzaType()))));

        for (Ingredient ingredient : Ingredient.values()) {
            if (pizzaPack.getIngredients().contains(ingredient)) {
                String left = String.format("%s", ingredient.toString());
                String right = String.format("%.2f €", Price.getIngredientPrice(ingredient));
                res.append(this.lineGenerator.floatToEnd(left, right));
            }
        }

        res.append(this.lineGenerator.divider());

        res.append(this.lineGenerator.floatToEnd("Всего:", String.format("%.2f €", pizzaPack.getSum())));
        res.append(this.lineGenerator.floatToEnd("Кол-во:", String.format("%d", pizzaPack.getQuantity())));

        res.append(this.lineGenerator.divider());

        return res.toString();
    }

    public String drawCheque(Order order) {
        StringBuilder res = new StringBuilder();
        res.append(this.lineGenerator.border());
        res.append(drawTitle(order));
        for (PizzaPack pizzaPack : order.getPizzas()) {
            res.append(drawPizzaPack(pizzaPack));
        }
        res.append(this.lineGenerator.floatToEnd("Общая сумма:", String.format("%.2f €", order.getTotal())));
        res.append(this.lineGenerator.border());
        return res.toString();
    }
}
