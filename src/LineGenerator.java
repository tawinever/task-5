public class LineGenerator {
    private int length;

    LineGenerator() {
        this.length = 33;
    }

    LineGenerator(int length) {
        this.length = length;
    }

    public String border() {
        return "*".repeat(Math.max(0, this.length - 1)) + "\n";
    }

    public String divider() {
        return "-".repeat(Math.max(0, this.length - 1)) + "\n";
    }

    public String floatToEnd(String left, String right) {
        String fill = " ".repeat(Math.max(0, this.length - left.length() - right.length()));
        return left + fill + right + "\n";
    }
}
