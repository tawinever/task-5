import java.time.LocalTime;
import java.util.Vector;

public class Order {
    private int orderId;
    private int clientId;
    private Vector<PizzaPack> pizzas;
    private LocalTime createdTime;

    Order(int orderId, int clientId, Vector<PizzaPack> pizzas) {
        if (orderId < 10000 || orderId > 99999)
            throw new IllegalArgumentException();
        this.orderId = orderId;
        this.clientId = clientId;
        this.pizzas = pizzas;
        updatePizzaName(0);
        this.createdTime = LocalTime.now();
    }

    void updatePizzaName(int index) {
        if (index >= pizzas.size())
            return;
        for (int i = index; i < pizzas.size(); ++i) {
            pizzas.get(i).setName(String.format("%d_%d", clientId, i + 1));
        }
    }

    public void removePizza(int index) {
        this.pizzas.remove(index);
        updatePizzaName(index);
    }

    public void setPizzaQuantity(int index, int quantity) {
        this.pizzas.get(index).setQuantity(quantity);
    }

    public void printOrder(int index) {
        System.out.println(String.format("%d : %d : %s : %d",
                orderId, clientId, pizzas.get(index).getName(), pizzas.get(index).getQuantity()));
    }

    public int getOrderId() {
        return this.orderId;
    }

    public int getClientId() {
        return this.clientId;
    }

    public Vector<PizzaPack> getPizzas() {
        return this.pizzas;
    }

    public double getTotal() {
        double total = 0;
        for (PizzaPack pizzaPack : this.pizzas) {
            total += pizzaPack.getSum() * pizzaPack.getQuantity();
        }
        return total;
    }


    @Override
    public String toString() {
        ChequeGenerator chequeGenerator = new ChequeGenerator();
        return chequeGenerator.drawCheque(this);
    }
}
