public class Price {
    static double getIngredientPrice(Ingredient ingredient) {
        switch (ingredient) {
            case TomatoPaste:
            case Cheese:
                return 1.0;
            case Salami:
                return 1.5;
            case Bacon:
                return 1.2;
            case Garlic:
                return 0.3;
            case Corn:
                return 0.7;
            case Pepperoni:
                return 0.6;
            case Olives:
                return 0.5;
            default:
                return 0.0;
        }
    }

    static double getBasePrice(PizzaType pizzaType) {
        if(pizzaType == PizzaType.Calzone)
            return 1.5;
        return 1.0;
    }
}
