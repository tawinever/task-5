import java.util.Vector;

public class Main {
    public static void main(String[] args) {
        Vector<PizzaPack> pizzas1 = new Vector<>();
        pizzas1.add(new PizzaPack("Margarita", PizzaType.Calzone, 2));
        pizzas1.add(new PizzaPack("«Pepperoni»", PizzaType.Classic, 3));

        pizzas1.get(0).addIngredient(Ingredient.TomatoPaste);
        pizzas1.get(0).addIngredient(Ingredient.Pepperoni);
        pizzas1.get(0).addIngredient(Ingredient.Garlic);
        pizzas1.get(0).addIngredient(Ingredient.Bacon);

        pizzas1.get(1).addIngredient(Ingredient.TomatoPaste);
        pizzas1.get(1).addIngredient(Ingredient.Cheese);
        pizzas1.get(1).addIngredient(Ingredient.Pepperoni);
        pizzas1.get(1).addIngredient(Ingredient.Olives);

        Order order1 = new Order(10001,7717, pizzas1);

        System.out.println(order1.toString());

        Vector<PizzaPack> pizzas2 = new Vector<>();
        pizzas2.add(new PizzaPack("«Base ZZ»", PizzaType.Classic, 12));

        pizzas2.get(0).addIngredient(Ingredient.TomatoPaste);
        pizzas2.get(0).addIngredient(Ingredient.Cheese);

        Order order2 = new Order(10002,4372, pizzas2);

        System.out.println(order2.toString());

    }
}
