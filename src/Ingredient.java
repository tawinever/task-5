public enum Ingredient {
    TomatoPaste,
    Cheese,
    Salami,
    Bacon,
    Garlic,
    Corn,
    Pepperoni,
    Olives
}
